# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# For debian wheezy dev
# $ GUEST_OS=debian7 vagrant up
# or (debian7 is default os)
# $ vagrant up
#
# For centos 6 dev
# $ GUEST_OS=centos6 vagrant up


Vagrant.configure("2") do |config|

# vagrant up will start everything in the right order

  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = "manifests"
    puppet.module_path = "modules"
    puppet.manifest_file = "zfs_nodes.pp"
    puppet.options = "--verbose --debug"
  end

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048", "--cpus", "2"]
  end

  # CentOS 6.5
  config.vm.define :centos65 do |centos65|
    centos65.vm.box = "puppetlabs/centos-6.5-64-puppet"
    centos65.vm.hostname = "centos65"
    centos65.vm.provider :virtualbox do |vb|
      vb.customize ["createhd",  "--filename", "centos65_disk0", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos65_disk1", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos65_disk2", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos65_disk3", "--size", "2048"]
      vb.customize ["storagectl", :id, "--name", "SATA Controller", "--add", "sata"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "1", "--type", "hdd", "--medium", "centos65_disk0.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "2", "--type", "hdd", "--medium", "centos65_disk1.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "3", "--type", "hdd", "--medium", "centos65_disk2.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "4", "--type", "hdd", "--medium", "centos65_disk3.vdi"]
    end
  end

  # CentOS 7.0
  config.vm.define :centos70 do |centos70|
    centos70.vm.box = "puppetlabs/centos-7.0-64-puppet"
    centos70.vm.hostname = "centos70"
    centos70.vm.provider :virtualbox do |vb|
      vb.customize ["createhd",  "--filename", "centos70_disk0", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos70_disk1", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos70_disk2", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "centos70_disk3", "--size", "2048"]
      vb.customize ["storagectl", :id, "--name", "SATA Controller", "--add", "sata"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "1", "--type", "hdd", "--medium", "centos70_disk0.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "2", "--type", "hdd", "--medium", "centos70_disk1.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "3", "--type", "hdd", "--medium", "centos70_disk2.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", "4", "--type", "hdd", "--medium", "centos70_disk3.vdi"]
    end
  end

    # Ubuntu Trusty
  config.vm.define :ubuntu14 do |ubuntu14|
    # added from Vagrant Cloud
    ubuntu14.vm.box = "ubuntu/trusty64"
    ubuntu14.vm.hostname = "ubuntu14"
    ubuntu14.vm.provider :virtualbox do |vb|
      vb.customize ["createhd",  "--filename", "ubuntu14_disk0", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "ubuntu14_disk1", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "ubuntu14_disk2", "--size", "2048"]
      vb.customize ["createhd",  "--filename", "ubuntu14_disk3", "--size", "2048"]
      vb.customize ["storageattach", :id, "--storagectl", "SATAController", "--port", "1", "--type", "hdd", "--medium", "ubuntu14_disk0.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATAController", "--port", "2", "--type", "hdd", "--medium", "ubuntu14_disk1.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATAController", "--port", "3", "--type", "hdd", "--medium", "ubuntu14_disk2.vdi"]
      vb.customize ["storageattach", :id, "--storagectl", "SATAController", "--port", "4", "--type", "hdd", "--medium", "ubuntu14_disk3.vdi"]
    end
  end

end
