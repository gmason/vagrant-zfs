node /centos/ {
  notify {'centos, the freshmaker':}
  include epel
  class {'zfs':
    require => Class['epel'],
  }
  include createpool
}
node /ubuntu/ {
  notify {'this is ubuntu':}
  include zfs
  include createpool
}

# creates pool for testing
class createpool {
  exec { 'createpool':
    path    => ['/bin','/sbin'],
    command => 'zpool create -f tank mirror sdb sdc mirror sdd sde',
    creates => '/tank',
    require => Class['zfs'],
  }
}
