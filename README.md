vagrant-zfs
===========

Description
-----------

This is a simple environment for testing changes to the gmason/puppet-zfs puppet
module.

To run, simply run `vagrant up`. It will create 2 VMs, one Ubuntu 12.04 and one
CentOS 6.4 VMs.

Setup
-----

After cloning, run `git submodule init` to pull down the latest version of the
ZFS module. You may also need to run `git submodule update`.

Run it
------

`vagrant up` will simply start up two VMs, install ZFS on Linux, then create a
zpool.

Further testing
---------------

TBD. Play around for now. Eventually will run OS updates to ensure DKMS is
configured properly.


Contributing
------------

Merge/pull requests via GitLab or GitHub are welcome. Please make a feature
branch with your change.
